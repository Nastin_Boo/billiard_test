using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CueController : MonoBehaviour
{
    [SerializeField]
    [Range(1, 10)]
    private float velocityModifier = 5;

    [SerializeField]
    [Range(1, 50)]
    private float maxDeltaVelocityModifier = 10;

    [SerializeField]
    public float fixedYPosition = 2.55f;

    [SerializeField]
    private float trajectoryLineWidth = 0.1f;

    private GameObject mainBall;
    private MainBallController mainBallController;

    private LineRenderer lineRenderer;

    private TouchPositionConverter touchPositionConverter = new TouchPositionConverter();

    private Vector3 endTouchPosition = Vector3.zero;

    public float Delta
    {
        get;
        private set;
    }

    public Vector3 CalculatedUnappliedVelocity
    {
        get;
        private set;
    }

    private void Start()
    {
        this.mainBallController = FindObjectOfType<MainBallController>();
        if (mainBallController != null)
        {
            this.mainBall = mainBallController.gameObject;
        }

        this.lineRenderer = this.GetComponent<LineRenderer>();
        if(this.lineRenderer != null)
        {
            this.InitializeTrajectoryRenderer();
        }
    }

    private void Update()
    {
        if (Input.touches.Length == 0
           || this.mainBall == null)
        {
            return;
        }

        if (Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            this.endTouchPosition = this.touchPositionConverter.ConvertTouchCoordinatesToWorld(
                Input.GetTouch(0).position, this.transform.position);
            this.endTouchPosition.y = this.fixedYPosition;
        }
        else if (Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            this.Delta = Vector3.Distance(this.endTouchPosition, this.mainBall.transform.position);

            var velocity = this.CalculateVelocity();
            this.mainBallController.ApplyVelocity(velocity);

            Vector3[] resetPositions = new Vector3[] { Vector3.zero };
            this.SetTrajectoryPositions(resetPositions);
            return;
        }

        this.Delta = Vector3.Distance(this.endTouchPosition, this.mainBall.transform.position);
        this.CalculatedUnappliedVelocity = this.CalculateVelocity();

        Vector3[] positions = new Vector3[] { this.mainBall.transform.position, this.endTouchPosition };
        this.SetTrajectoryPositions(positions);
    }

    private void InitializeTrajectoryRenderer()
    {
        this.lineRenderer.startWidth = this.trajectoryLineWidth;
        this.lineRenderer.endWidth = this.trajectoryLineWidth;
    }

    private Vector3 CalculateVelocity()
    {
        var velocityModifierTotal = Mathf.Min(this.Delta * this.velocityModifier, this.maxDeltaVelocityModifier);
        var velocity = -(this.endTouchPosition - this.mainBall.transform.position)
            * this.Delta * this.velocityModifier;
        velocity.y = 0;

        return velocity;
    }

    private void SetTrajectoryPositions(Vector3[] positions)
    {
        this.lineRenderer.SetPositions(positions);
        this.lineRenderer.positionCount = positions.Length;
    }   
}
