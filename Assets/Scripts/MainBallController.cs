using System;
using UnityEngine;

public class MainBallController : MonoBehaviour
{
    private Rigidbody ballRigidbody;

    public event EventHandler BallDestroyed;

    private void Start()
    {
        this.ballRigidbody = this.GetComponent<Rigidbody>();
    }

    public void ApplyVelocity(Vector3 velocity)
    {
        this.ballRigidbody.velocity = velocity;
    }

    public void DestroyBall()
    {
        if(this.BallDestroyed != null)
        {
            this.BallDestroyed(this, new EventArgs());
        }

        Destroy(this);
    }
}
