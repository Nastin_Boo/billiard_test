using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallTrajectoryRenderer : MonoBehaviour
{
    [SerializeField]
    [Range(0.1f, 1)]
    private float lineWidth = 0.1f;

    private Vector3 collision = Vector3.zero;

    private GameObject mainBall;

    private CueController cueController;

    private LineRenderer lineRenderer;

    private void Start()
    {
        this.lineRenderer = this.GetComponent<LineRenderer>();
        if(this.lineRenderer != null)
        {
            this.lineRenderer.startWidth = this.lineWidth;
            this.lineRenderer.endWidth = this.lineWidth;
        }

        var mainBallController = FindObjectOfType<MainBallController>();
        if (mainBallController != null)
        {
            this.mainBall = mainBallController.gameObject;
        }

        this.cueController = FindObjectOfType<CueController>();
    }

    private void Update()
    {
        if (Input.touches.Length == 0 || this.mainBall == null
            || this.cueController == null)
        {
            return;
        }

        if (Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            //
            // Erase previously drawn trajectory.
            //
            Vector3[] points = new Vector3[] { Vector3.zero };
            this.SetTrajectoryPositions(points);
            return;
        }
        
        this.DrawTrajectory(this.mainBall.transform.position, this.cueController.CalculatedUnappliedVelocity);
    }

    private void DrawTrajectory(Vector3 origin, Vector3 velocity)
    {
        Ray ray = new Ray(origin, velocity.normalized);

        RaycastHit rayHit;
        if (!Physics.Raycast(ray, out rayHit))
        {
            return;
        }

        this.collision = rayHit.point;

        Vector3[] points = new Vector3[] { origin, this.collision };
        this.SetTrajectoryPositions(points);
    }

    private void SetTrajectoryPositions(Vector3[] positions)
    {
        this.lineRenderer.SetPositions(positions);
        this.lineRenderer.positionCount = positions.Length;
    }
}
