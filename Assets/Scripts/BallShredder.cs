using UnityEngine;
using UnityEngine.SceneManagement;

public class BallShredder : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        var mainBallController = collision.gameObject.GetComponent<MainBallController>();
        if(mainBallController != null)
        {
            mainBallController.DestroyBall();
            return;
        }

        var ballController = collision.gameObject.GetComponent<BallController>();
        if(ballController != null)
        {
            Destroy(collision.gameObject);
        }
    }
}
