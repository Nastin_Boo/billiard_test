using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    private MainBallController mainBallController;

    private void Start()
    {
        this.mainBallController = FindObjectOfType<MainBallController>();
        if (mainBallController != null)
        {
            this.mainBallController.BallDestroyed += this.CueBallController_BallDestroyed;
        }
    }

    private void OnDisable()
    {
        if (this.mainBallController == null)
        {
            return;
        }

        this.mainBallController.BallDestroyed -= this.CueBallController_BallDestroyed;
    }

    private void CueBallController_BallDestroyed(object sender, System.EventArgs e)
    {
        if (mainBallController != null)
        {
            this.mainBallController.BallDestroyed -= this.CueBallController_BallDestroyed;
        }

        this.ReloadScene();
    }

    private void ReloadScene()
    {
        var currentScene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(currentScene.buildIndex);
    }
}
