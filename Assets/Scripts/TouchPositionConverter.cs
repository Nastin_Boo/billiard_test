using UnityEngine;

public class TouchPositionConverter
{
    public Vector3 ConvertTouchCoordinatesToWorld(Vector2 touchPosition, Vector3 rayCastPlanePosition)
    {
        Ray ray = Camera.main.ScreenPointToRay(touchPosition);

        Plane plane = new Plane(Vector3.up, rayCastPlanePosition);
        float distanceFromCamera;
        if (!plane.Raycast(ray, out distanceFromCamera))
        {
            return Vector3.zero;
        }

        return ray.GetPoint(distanceFromCamera);
    }
}
